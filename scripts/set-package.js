'use strict';
console.log('********************* ADICIONANDO SCRIPTS NO PACKAGE.JSON *********************');


const fs = require('fs');
const jsonBeautify = require("json-beautify");
let rawdata;
let path = '';
let projectName = '';
let filePath = '';
let isFileFound = false;

const generatePath = (index, pathRelative) => {
   if (index === 0) {
      return '';
   };
   pathRelative = `${ pathRelative }../`;
   return pathRelative;
};

const getFile = () => {
   for (let i = 0; i < 3; i++) {
      isFileFound = false;
      path = generatePath(i, path);
      filePath = `${ path }package.json`;
      isFileFound = fs.existsSync(filePath);
      if (isFileFound) {
         rawdata = fs.readFileSync(filePath);
         break;
      }
   }
   return JSON.parse(rawdata);
};

const addScript = () => {
   file.scripts = {
      "config": "ts-node ./src/set-env.js",
      "start": "npm run config -- --environment=dev && ng serve",
      "build": "npm run config && ng build --output-path=dist",
   }
};

const generateJSONFile = (file) => {
   return jsonBeautify(file, null, 2, 100);
};

const file = getFile();
addScript();
fs.writeFileSync(filePath, generateJSONFile(file));
