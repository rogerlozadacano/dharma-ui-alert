const fs = require('fs');
const yargs = require('yargs');

if (!yargs.argv.environment) {
    const dotenv = require('dotenv');

    try {
        if (fs.existsSync('../../.env')) {
            dotenv.config({ path: '../../.env' });
        } else {
            dotenv.config();
        }
    } catch (err) {
    }
}

const targetPath = `./src/environments/environment.ts`;
const envConfigFile = `export const environment = {
    production: true,
  };
`
fs.writeFile(targetPath, envConfigFile, function (err) {
    if (err) {
        console.log(err);
    }
    console.log(`Output generated at ${targetPath}`);
});

