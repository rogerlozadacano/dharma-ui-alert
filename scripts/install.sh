UNIX() {
    echo '************** Instalando dependencias... **************'
    echo '------> Script UNIX'
    sudo npm install bootstrap --save --unsafe-perm
    sudo npm install @coreui/coreui --save --unsafe-perm
    sudo npm install @coreui/icons --save --unsafe-perm
    sudo npm install popper.js --save --unsafe-perm
    sudo npm install jquery --save --unsafe-perm
    sudo npm install dotenv@6.2.0 --save --unsafe-perm
    node node_modules/dharma-ui-common/scripts/set-angular.js
    node node_modules/dharma-ui-common/scripts/set-package.js
    sudo cp node_modules/dharma-ui-common/scripts/set-env.js ./src
    echo
    echo '------> Script Finalizado'
}

WINDOWS() {
    echo '************** Instalando dependencias... **************'
    echo '------> Script windows'
    npm install bootstrap --save
    npm install @coreui/coreui --save
    npm install @coreui/icons --save
    npm install popper.js --save
    npm install jquery --save
    npm install dotenv@6.2.0
    node node_modules/dharma-ui-common/scripts/set-angular.js
    node node_modules/dharma-ui-common/scripts/set-package.js
    xcopy node_modules/dharma-ui-common/scripts/set-env.js ./src
    echo
    echo
    echo '------> Script Finalizado'
}

GUIDE() {
    echo
    echo
    echo '************** dharma-ui-install **************'
    echo
    echo
}

GUIDE;
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     UNIX;;
    Darwin*)    UNIX;;
    CYGWIN*)    WINDOWS;;
    MINGW*)     WINDOWS;;
    *)          UNIX;;
esac
echo ${machine}
