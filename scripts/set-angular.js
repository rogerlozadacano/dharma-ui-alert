'use strict';
console.log('********************* ADICIONANDO MODULOS NO ANGULAR.JSON *********************');


const fs = require('fs');
const jsonBeautify = require("json-beautify");
let rawdata;
let path = '';
let projectName = '';
let filePath = '';
let isFileFound = false;

const generatePath = (index, pathRelative) => {
   if (index === 0) {
      return '';
   };
   pathRelative = `${ pathRelative }../`;
   return pathRelative;
};

const getFile = () => {
   for (let i = 0; i < 3; i++) {
      isFileFound = false;
      path = generatePath(i, path);
      filePath = `${ path }angular.json`;
      isFileFound = fs.existsSync(filePath);
      if (isFileFound) {
         rawdata = fs.readFileSync(filePath);
         break;
      }
   }
   return JSON.parse(rawdata);
};

const getProject = (file) => {
   const projects = Object.keys(file.projects);
   let project;
   projects.forEach(projectKey => {
      const projectTest = file.projects[projectKey];
      try {
         if (projectTest.architect && projectTest.architect.build) {
            projectName = projectKey;
            project = file.projects[projectKey];
         }
      } catch(err) {}
   });
   return project;
};

const checkIfExist = (assets, asset) => {
   return assets.includes(asset);
};

const addStyles = (project) => {
   const styleObject = JSON.parse(fs.readFileSync('node_modules/dharma-ui-common/scripts/files.json'));
   const styles = styleObject.styles;
   styles.forEach(style => {
      const assets = project.architect.build.options.styles;
      checkIfExist(assets, style) ? null : project.architect.build.options.styles.push(style);
   });
   file.projects[projectName] = project;
};

const addScript = (project) => {
   const scriptObject = JSON.parse(fs.readFileSync('node_modules/dharma-ui-common/scripts/files.json'));
   const scripts = scriptObject.scripts;
   scripts.forEach(script => {
      const assets = project.architect.build.options.scripts;
      checkIfExist(assets, script) ? null : project.architect.build.options.scripts.push(script);
   });
   file.projects[projectName] = project;
};

const generateJSONFile = (file) => {
   return jsonBeautify(file, null, 2, 100);
};

const file = getFile();
const project = getProject(file);
addStyles(project);
addScript(project);
fs.writeFileSync(filePath, generateJSONFile(file));

console.log('>>>>>>>> Script finalizado');
