UNIX() {
    echo '************** Copiando arquivos... **************'
    echo 'Script UNIX'
    sudo cp -R scripts dist
    sudo chmod +x dist/scripts/install.sh
    sudo chmod +x dist/scripts/set-angular.js
    echo 'Scripts injetados na dist'
    echo 'path: dist/scripts'
}

WINDOWS() {
    echo '************** Copiando arquivos... **************'
    echo 'Script windows'
    mkdir dist/scripts
    xcopy scripts dist/scripts
    echo 'Scripts injetados na dist'
    echo 'path: dist/scripts'
}

GUIDE() {
    echo
    echo
    echo '************** Injetando scripts na dist **************'
    echo
    echo
}

GUIDE;
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     UNIX;;
    Darwin*)    UNIX;;
    CYGWIN*)    WINDOWS;;
    MINGW*)     WINDOWS;;
    *)          UNIX;;
esac
echo ${machine}
