const fs = require('fs');
const yargs = require('yargs');

if (!yargs.argv.environment) {
    const dotenv = require('dotenv');

    try {
        if (fs.existsSync('../../.env')) {
            dotenv.config({ path: '../../.env' });
        } else {
            dotenv.config();
        }
    } catch (err) {
    }
}

const targetPath = `./src/environments/environment.ts`;
const envConfigFile = `export const environment = {
    production: true,
    accounts_api: '${process.env.SSO_DEFAULT_URI_ACCOUNTS}',
    site_dotz: '${process.env.SITE_DOTZ}',
    salesForce: '${process.env.SALESFORCE}',
    siteKeyRecaptcha: '${process.env.SITEKEYRECAPTCHA}',
    RECAPTCHA_ENABLED: ${process.env.RECAPTCHA_ENABLED},
    helpDesk_api: '${process.env.HELPDESK_API}',
  };
`
fs.writeFile(targetPath, envConfigFile, function (err) {
    if (err) {
        console.log(err);
    }
    console.log(`Output generated at ${targetPath}`);
});

