import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertSummaryService } from './alert-summary.service';
import { Observable, Subject } from 'rxjs';

export interface AlertOptions {
  type: string,
  title: string,
  message: any,
  timeout?: number,
};

@Component({
  selector: 'dharma-ui-alert-summary',
  templateUrl: './alert-summary.component.html',
  styleUrls: ['./alert-summary.component.scss']
})
export class AlertSummaryComponent implements OnInit, OnDestroy {
  
  show: boolean = false;
  errors: Array<string> = [];
  AlertOptions: AlertOptions; 

  devErrors = new Subject();
  devErrorsAsync: Observable<any> = this.devErrors.asObservable();

  constructor(
    private service: AlertSummaryService,
  ) { }

  showAlert(alertOptions: AlertOptions) {
    this.show = (alertOptions.hasOwnProperty('message') && alertOptions['message'] !== '');
  }

  parseMessage(alertOptions: AlertOptions) {
    if (!alertOptions.hasOwnProperty('message') || alertOptions['message'] === '') {
      return;
    }
    if (typeof alertOptions.message === 'string') {
      this.show = true;
      this.AlertOptions = alertOptions;
      return;
    }

    this.AlertOptions = alertOptions;

    if (typeof alertOptions.message === 'string') { return; }

    const generateErrorList = (message): string => {
      let listError = '';
      const keys = Object.keys(message);
      const errorReference = keys.filter(key => key === 'Errors' || key === 'errors')[0];
      if (message && message[errorReference] && message[errorReference].length === 0) {
        console.warn('Dharma Alert Summary - Empty message');
        this.show = false;
        return;
      }

      if (message && message[errorReference]) {
        try {
          message[errorReference].forEach(error => {
            const errorKeys = Object.keys(error);
            const messageReference = errorKeys.filter(key => key === 'Message' || key === 'message')[0];
            const errorMessage = error[messageReference];
            if (this.errors.includes(errorMessage)) { return; }
            listError = `${listError}<li>${ errorMessage }</li>`;
            this.errors.push(errorMessage);
          });
        } catch(err) {
          this.devErrors.next({
            code: 900,
            typeError: 'DHARMA UI ALERT ERROR - Incorrect Payload server error',
            message: 'Favor utilizar o padrão Dotz de requisição, Exemplo: {"Errors":[{"Code":"000","Message":"INVALID_NAME"},{"Code":"001","Message":"INVALID_AGE"}],"Success":false}',
            success: false,
            type: 'warn',
          });
        }
      } else {
        try { listError = `<li>${JSON.stringify(message)}</li>`; } catch(err) { listError = `<li>${message}</li>` };
        this.devErrors.next({
          code: 900,
          typeError: 'DHARMA UI ALERT ERROR - Incorrect Payload server error',
          message: 'Favor utilizar o padrão Dotz de requisição, Exemplo: {"Errors":[{"Code":"000","Message":"INVALID_NAME"},{"Code":"001","Message":"INVALID_AGE"}],"Success":false}',
          success: false,
          type: 'warn',
        });
      }
      this.showAlert(alertOptions);
      return listError;
    };
    const message: any = this.AlertOptions.message;
    const returnMessage: string = `<ul>${ generateErrorList(message) }</ul>`;
    this.AlertOptions.message = returnMessage;
  }

  ngOnInit(): void {
    this.devErrorsAsync.subscribe(error => { console[error.type](error.typeError, error); });
    this.service.messageAsync.subscribe((options: AlertOptions) => {
      if (options && options.type === 'hide') {
        this.show = false;
        this.AlertOptions = null;
        return;
      }
      this.parseMessage(options);
      setTimeout(() => {
        this.show = false;
      }, options.timeout);
    });
  }

  ngOnDestroy(): void {
    this.show = false;
    if (this.AlertOptions && this.AlertOptions.message) {
      this.AlertOptions.message = null;
    }
  }

}
