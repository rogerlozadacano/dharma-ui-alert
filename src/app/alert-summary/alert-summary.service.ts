import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

/**
 * @description Service para mostrar um alert de erro ou de aviso
 * @example Erro this.alertSummary.danger('teste', errorList);
 * @example Aviso this.alertSummary.warning('teste', errorList);
 */
@Injectable()
export class AlertSummaryService {

  message = new BehaviorSubject<Object>({});
  messageAsync = this.message.asObservable();

  /**
   * @description Propriedade para mostrar o alert warning
   * @param { string } title Pode ser uma string ou um objeto html
   * @param { string } message Pode ser uma string ou um objeto html
   * @param { boolean } timeout Tempo que o alert irá permanecer na tela
   * @example this.AlertSummaryService.warning('teste', 'texto');
   * @example this.AlertSummaryService.warning('teste', '<b>This message is bold</b>', 2000);
   */
  warning (title: string, message: string, timeout = 10000): void {
    this.message.next({
      type: `alert-warning`,
      title,
      message,
      timeout,
    });
  }

  /**
   * @description Propriedade para mostrar o alert danger
   * @param { string } title Pode ser uma string ou um objeto html
   * @param { string } message Pode ser uma string ou um objeto html
   * @param { boolean } timeout Tempo que o alert irá permanecer na tela
   * @example this.AlertSummaryService.danger('teste', 'texto');
   * @example this.AlertSummaryService.danger('teste', '<b>This message is bold</b>', 2000);
   */
  danger(title: string, message: any, timeout = 10000): void {
    this.message.next({
      type: `alert-danger`,
      title,
      message,
      timeout,
    });
  }

  hide() {
    this.message.next({
      type: `hide`,
    });
  }

  isCorrectPayload(message): boolean {
    const keys = Object.keys(message);
    const errorReference = keys.filter(key => key === 'Errors' || key === 'errors')[0];
    if (message && message[errorReference] && message[errorReference].length === 0) {
      return false;
    }

    if (message && message[errorReference]) {
      try {
        message[errorReference].forEach(error => {
          const errorKeys = Object.keys(error);
          const messageReference = errorKeys.filter(key => key === 'Message' || key === 'message')[0];
          const errorMessage = error[messageReference];
        });
        return true;
      } catch(err) {
        return false;
      }
    } else {
      return false;
    }
  }
}
