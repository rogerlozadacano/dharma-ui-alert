import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertSummaryComponent } from './alert-summary.component';
import { AlertSummaryService } from './alert-summary.service';
 

@NgModule({
  declarations: [AlertSummaryComponent],
  imports: [
    CommonModule
  ],
  providers: [AlertSummaryService],
  exports: [AlertSummaryComponent],
})
export class AlertSummaryModule { }
