import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExampleComponent } from './example.component';
import { RouterModule } from '@angular/router';
import { ExampleRoutes } from './example.routing';
import { AlertSummaryModule } from './../alert-summary/alert-summary.module';
import { AlertModule } from '../libraries/ngx-alerts/alert.module';
import { SweetAlertModule } from '../sweet-alert/sweet-alert.module';

@NgModule({
  declarations: [ExampleComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ExampleRoutes),
    AlertModule,
    AlertSummaryModule,
    SweetAlertModule,
  ]
})
export class ExampleModule { }
