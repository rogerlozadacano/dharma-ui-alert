import { Component, OnInit } from '@angular/core';
import { AlertSummaryService } from '../alert-summary/alert-summary.service';
import { AlertService } from '../libraries/ngx-alerts/service/alert.service';
import { SweetAlertService } from '../sweet-alert/sweet-alert.service';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnInit {

  constructor(
    private alertService: AlertService,
    private alertSummary: AlertSummaryService,
    private sweetAlertService: SweetAlertService,
  ) { }

  ngOnInit() {
    this.alertService.danger('danger', { timeout: 10000 });
    this.alertService.success('success', { timeout: 10000 });
    this.alertService.info('info', { timeout: 10000 });
    this.alertService.warning({ html: '<p>oi</p>'}, { timeout: 10000 });
    this.sweetAlertService.show({
      title: 'teste',
      text: 'teste 123',
    });
    const error = {
      "errors": [
        {
          "code": "197",
          "message": "Não foi possível confirmar o código informado, tente novamente ou solicite outro código."
        }
      ],
      "success": false
    };
    const isCorrectPayload = this.alertSummary.isCorrectPayload(error);
    console.log('isCorrectPayload', isCorrectPayload);
    this.alertSummary.danger('teste', { NAME: 'diego'}, 150000);
  }

}
