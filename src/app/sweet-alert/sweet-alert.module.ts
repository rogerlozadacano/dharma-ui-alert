import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SweetAlertService } from './sweet-alert.service';

@NgModule({
  imports: [
    CommonModule
  ],
})
export class SweetAlertModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SweetAlertModule,
      providers: [
        SweetAlertService,
      ]
    };
  };
  static forChild(): ModuleWithProviders {
    return {
      ngModule: SweetAlertModule,
      providers: [
        SweetAlertService,
      ]
    };
  };
}
