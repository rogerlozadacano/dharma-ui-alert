export interface sweetAlertArgs {
  title: string,
  text?: string,
  icon?: 'success' | 'error' | 'warning' | 'info' | 'question',
  showCancelButton?: boolean,
  confirmButtonText?: string,
  cancelButtonText?: string,
  html?: string | HTMLElement | JQuery,
  position?: 'top' | 'top-start' | 'top-end' | 'top-left' | 'top-right' |
  'center' | 'center-start' | 'center-end' | 'center-left' | 'center-right' |
  'bottom' | 'bottom-start' | 'bottom-end' | 'bottom-left' | 'bottom-right',
};