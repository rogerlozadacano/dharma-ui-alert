import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { from } from 'rxjs';
import { sweetAlertArgs } from './sweet-alert.interface';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  private args: sweetAlertArgs = {
    title: null,
    text: null,
    icon: 'success',
    showCancelButton: false,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancelar',
    html: null,
    position: 'center',
  };

  /**
   * @description Função para mostrar um popup-alert
   * @example export interface sweetAlertArgs {
        title: string, 
        text?: string, 
        icon: 'success' | 'error' | 'warning' | 'info' | 'question', 
        showCancelButton: boolean, 
        confirmButtonText: string, 
        cancelButtonText: string, 
        html?: string | HTMLElement | JQuery, 
        position: 'top' | 'top-start' | 'top-end' | 'top-left' | 'top-right' |
        'center' | 'center-start' | 'center-end' | 'center-left' | 'center-right' |
        'bottom' | 'bottom-start' | 'bottom-end' | 'bottom-left' | 'bottom-right',
      };
   * @example this.sweetAlertService.show({
      title: null, 
      text: null, 
      icon: 'success', 
      showCancelButton: false, 
      confirmButtonText: 'OK', 
      cancelButtonText: 'Cancelar', 
      html: null, 
      position: 'center',
    });
   */
  public show(args: sweetAlertArgs) {
    this.args = { ...this.args, ...args };
    return from(Swal.fire(this.args));
  }
}
