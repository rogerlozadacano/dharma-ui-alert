import { Injectable } from '@angular/core';
import {Alert} from '../model/alert.model';
import {BehaviorSubject, Observable, Subject, timer} from 'rxjs';
import {AlertConfig} from '../model/alert-config.model';
import {scan, take} from 'rxjs/internal/operators';
import {AlertReducer} from './alert.reducer';

@Injectable()
export class AlertService {

  private dispatcher = new Subject<{ fn: Function, alert: Alert, config: AlertConfig }>();
  private state = new BehaviorSubject<Alert[]>([]);
  private config: AlertConfig

  constructor() {
    this.initConfig();
    this.dispatcher
      .pipe(
        scan(AlertReducer.reduce, [])
      )
      .subscribe(this.state);
  }

  private initConfig(): void {
    if (!this.config) {
      this.config = {};
    }
    this.config.timeout = !!this.config.timeout ? this.config.timeout : 20000;
    this.config.maxMessages = !!this.config.maxMessages ? this.config.maxMessages : 2000;
  }

  public get messages(): Observable<Alert[]> {
    return this.state.asObservable();
  }

  /**
   * 
   * @param msg Pode ser uma string ou um html
   * @param options 
   * @example this.alertService.info('warning', { timeout: 10000 });
   * @example this.alertService.info({ html: '<p>oi</p>'}, { timeout: 10000 });
   */
  public info(msg: string | { html: string }, options?: AlertConfig): void {
    this.addAlert({content: msg, type: 'info'}, options);
  }

  /**
   * 
   * @param msg Pode ser uma string ou um html
   * @param options 
   * @example this.alertService.danger('warning', { timeout: 10000 });
   * @example this.alertService.danger({ html: '<p>oi</p>'}, { timeout: 10000 });
   */
  public danger(msg: string | { html: string }, options?: AlertConfig): void {
    this.addAlert({content: msg, type: 'danger'}, options);
  }

  /**
   * 
   * @param msg Pode ser uma string ou um html
   * @param options 
   * @example this.alertService.success('warning', { timeout: 10000 });
   * @example this.alertService.success({ html: '<p>oi</p>'}, { timeout: 10000 });
   */
  public success(msg: string | { html: string }, options?: AlertConfig): void {
    this.addAlert({content: msg, type: 'success'}, options);
  }

  /**
   * 
   * @param msg Pode ser uma string ou um html
   * @param options 
   * @example this.alertService.warning('warning', { timeout: 10000 });
   */
  public warning(msg: string | { html: string }, options?: AlertConfig): void {
    this.addAlert({content: msg, type: 'warning'}, options);
  }

  public close(alert: Alert): void {
    this.dispatcher.next({fn: AlertReducer.remove, alert: alert, config: this.config});
  }

  private addAlert(alert: Alert, options: AlertConfig): void {
    this.config = { ...this.config, ...options };
    this.dispatcher.next({fn: AlertReducer.add, alert: alert, config: this.config});

    if (this.config.timeout > 0) {
      timer(this.config.timeout)
        .pipe(take(1))
        .subscribe(() => {
          this.dispatcher.next({fn: AlertReducer.remove, alert: alert, config: this.config});
        });
    }
  }
}
