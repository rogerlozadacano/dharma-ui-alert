
# dharma-ui-alert

[![NPM](https://nodei.co/npm/dharma-ui-alert.png?downloads=true&downloadRank=true&stars=true)](https://www.npmjs.com/package/dharma-ui-alert/)


### Motivação:

O dharma UI alert tem como objetivo facilitar o uso dos alerts dentro da aplicação e utilizar o modelo ideal para cada situação.

  

O que o Dharma Alert oferece:

- Alert Summary

- Alert

  

### Sumário:

<div id="TOC">

<ul>

<li>

<a href="#initial-config">Configurações iniciais</a>

</li>

<li>

<a href="#alert-summary">Alert Summary</a>

</li>

<li>

<a href="#alert">Alert</a>

</li>

<li>

<a href="#contribute">Como contribuir?</a>

</li>

<li>

<a href="#git-flow-contribute">Git Flow para contribuir</a>

</li>

<li>

<a href="#responsible">Responsáveis</a>

</li>

</ul>

</div>

  

<div id="initial-config">

<h2>Configurações iniciais</h2>

</div>

  

Instale o módulo

```

$ npm install dharma-ui-alert --save

```

Para utilizar o `dharma-ui-alert` é necessário utilizar as seguintes versões:

  

```json
{

	"dependencies": {
    "@angular/animations": "~8.2.12",
    "@angular/common": "~8.2.12",
    "@angular/compiler": "~8.2.12",
    "@angular/core": "~8.2.12",
    "@angular/forms": "~8.2.12",
    "@angular/platform-browser": "~8.2.12",
    "@angular/platform-browser-dynamic": "~8.2.12",
    "@angular/router": "~8.2.12",
	},
	"devDependencies": {
    "@angular-devkit/build-angular": "~0.803.14",
    "@angular/cli": "~8.3.14",
    "@angular/compiler-cli": "~8.2.12",
    "@angular/language-service": "~8.2.12",
    "@types/node": "~12.11.6",
    "@types/jasmine": "~3.4.4",
    "@types/jasminewd2": "~2.0.3",
    "codelyzer": "^5.0.1",
    "jasmine-core": "~3.5.0",
    "jasmine-spec-reporter": "~4.2.1",
    "karma": "~4.4.1",
    "karma-chrome-launcher": "~3.1.0",
    "karma-coverage-istanbul-reporter": "~2.1.0",
    "karma-jasmine": "~2.0.1",
    "karma-jasmine-html-reporter": "^1.4.2",
    "protractor": "~5.4.0",
    "ts-node": "~8.4.1",
    "tslint": "~5.20.0",
    "typescript": "~3.5.0"
	}

}

```

  

<div id="alert-summary">

<h2>Alert Summary</h2>

</div>

  

Descrição: O `alert-summary` tem como objetivo mostrar os erros que vem do servidor, como por exemplo: *erro de formulários*.

  

Observação: É necessário a utilização

  

Existe os seguintes tipos de `alert-summary`:

- Danger

- Warning

<a href="https://ibb.co/nk48SBv"><img src="https://i.ibb.co/VBbp8xZ/Captura-de-Tela-2019-11-18-a-s-15-42-23.png" alt="Captura-de-Tela-2019-11-18-a-s-15-42-23" border="0"></a>

<a href="https://ibb.co/9prhLZh"><img src="https://i.ibb.co/Jcsq43q/Captura-de-Tela-2019-11-18-a-s-15-42-54.png" alt="Captura-de-Tela-2019-11-18-a-s-15-42-54" border="0"></a>

  
  

Para mostrar o erro na tela, basta importar `AlertSummaryService` e o `<dharma-ui-alert-summary>` em seu componente e o `alertSummaryModule` no módulo que irá utilizar.

  
  

Abaixo temos um exemplo no qual iremos chamar a service `minhaService` e mostrar o alert-summary quando ocorrer algum erro.

  

No component:

```javascript
import { Component, OnInit } from '@angular/core';
import { Menu } from 'dharma-ui-common';
import { AlertSummaryService } from 'dharma-ui-alert';
import { minhaService } from 'meu-modulo';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	constructor(
	private alertSumaryService: AlertSummaryService,
	private minhaService: minhaService,
	) {

	ngOnInit() {
		this.minhaService.getUsers().subscribe(users => {

		}, (err) => {
			this.alertSumaryService.danger('Erro', err);
		});
	}
}

```

  

Para configurar um **timeout** no alert basta adicionar um terceiro parâmetro como no exemplo abaixo:

```javascript
this.alertSumaryService.danger('Erro', err, 2000);
```

No HTML adicione a tag `<dharma-ui-alert-summary>`

```html

<dharma-ui-alert-summary></dharma-ui-alert-summary>

```

  

No módulo importe o `alertSummaryModule`

  

```javascript

import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AlertSummaryModule } from 'dharma-ui-alert';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		AlertSummaryModule,
	],
})
export class AppModule {}

```

  

<div id="alert">

<h2>Alert</h2>

</div>

  

Descrição: O `alert` tem como objetivo mostrar os erros que vem do servidor, como por exemplo: *erro de formulários*.

  

Existe os seguintes tipos de `alert`:

- Danger

- Warning

- info

- Success

<a href="https://imgbb.com/"><img src="https://i.ibb.co/S6CsCHf/Captura-de-Tela-2019-11-19-a-s-11-14-50.png" alt="Captura-de-Tela-2019-11-19-a-s-11-14-50" border="0"></a>


Para mostrar o erro na tela, basta importar `AlertService` e o `<dharma-ui-alert>` em seu componente e o `alertModule` no módulo que irá utilizar.


No component:

```javascript
import { Component, OnInit } from '@angular/core';
import { Menu } from 'dharma-ui-common';
import { AlertService } from 'dharma-ui-alert';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

	constructor(
		private alertService: AlertService,
	) {

	ngOnInit() {
		this.alertService.danger('danger', { timeout: 10000 });
		this.alertService.success('success', { timeout: 10000 });
		this.alertService.info('info', { timeout: 10000 });
		this.alertService.warning('warning', { timeout: 10000 });
	}

}

```

  

No HTML adicione a tag `<dharma-ui-alert>`

```html

<dharma-ui-alert></dharma-ui-alert>

```

  

No módulo importe o `alertModule`

  

```javascript

import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AlertModule } from 'dharma-ui-alert';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
  AlertModule,
  ],
})

export class AppModule {}
```

  <div id="alert">

<h2>Sweet Alert</h2>

</div>

  Descrição: O `sweetAlert` tem como objetivo mostrar um popup, podendo ter botões de confirm e cancel.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/djbhZkk/Captura-de-Tela-2019-12-05-a-s-13-55-41.png" alt="Captura-de-Tela-2019-12-05-a-s-13-55-41" border="0"></a>


Argumentos disponíveis:
| Argumento | Tipo(s) | Valor Default|
|--|--|--|
| title | string | null
| text | string | null
| icon | 'success'/'error'/'warning'/'info'/'question' | 'success'
| showCancelButton | boolean |false
| confirmButtonText | string | 'OK'
| html | string/HTMLElement/JQuery | null
| position | 'top'/'top-start'/'top-end'/'top-left'/'top-right'/'center'/'center-start'/'center-end'/'center-left'/'center-right'/'bottom' /'bottom-start' /'bottom-end' /'bottom-left' /'bottom-right', | 'center'




No módulo importe o `alertModule`

  

```javascript
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AlertModule } from 'dharma-ui-alert';

@NgModule({
	declarations: [
	  AppComponent
	],
	imports: [
	 AlertModule,
	],
})
export class AppModule {}

```

No component:

```javascript
import { SweetAlertService } from  'dharma-ui-alert';

@Component({
	selector: 'app-example',
	templateUrl: './example.component.html',
	styleUrls: ['./example.component.scss']
})
export  class  ExampleComponent  implements  OnInit {

	constructor(
		private alertService: AlertService,
		private alertSummary: AlertSummaryService,
		private sweetAlertService: SweetAlertService,
	) {}

	ngOnInit() {
		this.sweetAlertService.show({
			title: 'teste',
			text: 'teste 123',
		});
	}
}
```
Observação: O sweetAlert não necessita importar nada no HTML.


<div id="contribute">

<h2> Como contribuir? </h2>

</div>

  

Todos os desenvolvedores são bem vindos em contribuir com a comunidade Dotz, basta abrir um `pull request` para os nossos arquitetos avaliarem e juntos encontrarem a melhor maneira de implementar sua ideia.

  
  

<div id="git-flow-contribute">

<h2> Git Flow para contribuir </h2>

</div>

  

- Commits atômicos, que resolvem no máximo uma tarefa do board.

- Mensagem do commit deve seguir o que é sugerido no README.md do Dharma.Common: [https://github.com/stone-payments/stoneco-best-practices/blob/master/gitStyleGuide/README.md#commits](https://github.com/stone-payments/stoneco-best-practices/blob/master/gitStyleGuide/README.md#commits).

- Em cada PR, devemos escrever nos comentários o motivo de cada arquivo alterado (ex: TransfersController recebe novo filtro no método Get()).

- Só integrar PRs que tenham sido aprovadas **no mínimo** por 2 outros devs que não sejam o autor do PR, e preferencialmente todos nós devemos aprovar o PR para termos sempre certeza que todos estão cientes das mudanças e tenham feito ressalvas caso algum impacto tenha sido gerado.

- Vamos manter a organização das branches no modelo parecido com gitflow (Master; Bugfix-[Numero bug]; Dev-[nome Release];), sempre sincronizadas com a master para caso precisarmos entregar na esteira.

  
  

<div id="responsible">

<h2> Responsáveis técnicos </h2>

</div>

  

- Roger Louzada - roger.cano@dotz.com

- Diego Sanches - diego.sanches@dotz.com

- Samuel Fabel - samuel.fabel@dotz.com